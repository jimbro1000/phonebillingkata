let HashMap = require("hashmap");
let self = module.exports = {
    calculateBill: function (input) {
        let lineSplitRegex = /[\n]/;
        let band1 = 3;
        let band2 = 150;
        let band2boundary = 4;
        let result = 0;
        let dictionary = new HashMap();
        if (input && input !== "") {
            input.split(lineSplitRegex).forEach(function(item) {
                let entry = self.decodeInput(item);
                let minutes = entry.duration.minutes;
                let duration = minutes * 60 + entry.duration.seconds;
                let charge = 0;
                if (minutes > band2boundary) {
                    if (entry.duration.seconds > 0) {
                        ++minutes;
                    }
                    charge = minutes * band2;
                } else {
                    charge = (minutes * 60 + entry.duration.seconds) * band1;
                }
                if (dictionary.has(entry.phoneNumber)) {
                    dictionary.get(entry.phoneNumber).add(duration, charge);
                } else {
                    dictionary.set(entry.phoneNumber, new self.BillObject(entry.phoneNumber, duration, charge));
                }
            });
            let calls = dictionary.values().sort(function(a, b) {
                if (a.duration === b.duration) {
                    return b.phoneNumber - a.phoneNumber;
                } else {
                    return a.duration - b.duration;
                }
            });
            for(let index=0; index<calls.length - 1; ++index) {
                result += calls[index].charge;
            }
        }
        return result;
    },
    decodeInput: function (input) {
        let duration, phoneNumber, minutes, seconds;
        let lineSplitRegex = /[,]/;
        let numberSplitRegex = /[-]/;
        let timeSplitRegex = /[:]/;
        let parts = input.split(lineSplitRegex);
        duration = parts[0];
        phoneNumber = parts[1];
        parts = phoneNumber.split(numberSplitRegex);
        phoneNumber = 0;
        for (let i=0;i<parts.length;++i) {
            phoneNumber = 1000 * phoneNumber + Number(parts[i]);
        }
        parts = duration.split(timeSplitRegex);
        minutes = Number(parts[0]) * 60 + Number(parts[1]);
        seconds = Number(parts[2]);
        return {duration: {minutes: minutes, seconds: seconds}, phoneNumber: phoneNumber};
    },
    BillObject: function(phoneNumber, duration, charge) {
        this.phoneNumber = phoneNumber;
        this.duration = duration;
        this.charge = charge;
        this.calls = [{duration: duration, charge: charge}];

        this.add = function(duration, charge) {
            this.calls.push({duration: duration, charge: charge});
            this.charge += charge;
            if (this.duration < duration) {
                this.duration = duration;
            }
        };
    }
};
