const expect = require("chai").expect;
let phoneBillingPath = "../app/phoneBilling.js";

describe("Phone Billing", () => {
    it("exists", () => {
        expect(require(phoneBillingPath)).to.exist;
    });

    describe("calculateBill", () => {
        let calculateBill;
        beforeEach(() => {
            calculateBill = require(phoneBillingPath).calculateBill;
        });
        it("exists", () => {
            expect(calculateBill).to.exist;
        });
        it("returns the value of the bill as a number", () => {
            expect(typeof calculateBill()).to.equal("number");
        });
        it("charges 3 cents per second", () => {
            expect(calculateBill("00:00:01,123-456-789\n00:00:02,987-654-321")).to.equal(3);
            expect(calculateBill("00:01:00,123-456-789\n00:02:02,987-654-321")).to.equal(180);
        });
        it("charges 150 cents per minute (rounded up) for calls over 5 minutes", () => {
            expect(calculateBill("00:04:59,123-456-789\n00:05:02,987-654-321")).to.equal(897);
            expect(calculateBill("00:05:00,123-456-789\n00:05:02,987-654-321")).to.equal(750);
            expect(calculateBill("00:05:01,123-456-789\n00:05:02,987-654-321")).to.equal(900);
        });
        it("accepts multiple newline separated items and totals the items", () => {
            expect(calculateBill("00:05:00,123-456-789\n00:04:00,987-654-321\n00:05:02,789-654-321")).to.equal(1470);
        });
        it("charges nothing to the phone number with the longest total call duration", () => {
            expect(calculateBill("00:05:00,123-456-789\n00:04:00,987-654-321")).to.equal(720);
            expect(calculateBill("00:03:00,123-456-789\n00:03:00,123-456-789\n00:05:00,987-654-321")).to.equal(1080);
        });
        it("resolves a conflict on the longest call by accepting only the lowest numeric phone number for the discount", () => {
            expect(calculateBill("00:03:00,123-456-789\n00:06:00,987-654-321\n00:03:00,123-456-789")).to.equal(1080);
        });
    });

    describe("decodeInput", () => {
        let decodeInput;
        beforeEach(() => {
            decodeInput = require(phoneBillingPath).decodeInput;
        });
        describe("duration", () => {
            it("accepts a duration in the format 'hh:mm:ss' and returns the time as minutes and seconds", () => {
                expect(decodeInput("12:34:56,123-456-789").duration.minutes).to.equal(754);
                expect(decodeInput("12:34:56,123-456-789").duration.seconds).to.equal(56);
                expect(decodeInput("00:00:01,123-456-789").duration.seconds).to.equal(1);
                expect(decodeInput("01:00:00,123-456-789").duration.minutes).to.equal(60);
                expect(decodeInput("00:01:00,123-456-789").duration.minutes).to.equal(1);
            });
        });
        describe("phoneNumber", () => {
            it("accepts phone number in the format 'nnn-nnn-nnn' and returns the phone number as an integer", () => {
                expect(decodeInput("12:34:56,123-456-789").phoneNumber).to.equal(123456789);
            });
            it("ignores leading zeros in the phone number", () => {
                expect(decodeInput("12:34:56,012-345-678").phoneNumber).to.equal(12345678);
            });
        });
    });
});

describe("billObject", () => {
    let BillObject;
    beforeEach(() => {
        BillObject = require(phoneBillingPath).BillObject;
    });
    it("exists", () => {
        expect(BillObject).to.exist;
    });
    describe("constructor", () => {
        it("accepts phone number, duration and charge as parameters", () => {
            let phoneNumber=123456789;
            let duration=250;
            let charge=750;
            let test = new BillObject(phoneNumber, duration, charge);
            expect(test.phoneNumber).to.equal(phoneNumber);
            expect(test.charge).to.equal(charge);
            expect(test.duration).to.equal(duration);
        });
        describe("add", () => {
            let test;
            let phoneNumber;
            beforeEach(() => {
                phoneNumber=123456789;
                test = new BillObject(phoneNumber, 1, 3);
            });
            it("exists", () => {
                expect(test.add).to.exist;
            });
            it("accepts the duration and charge as parameters", () => {
                test.add(10,30);
                expect(test.charge).to.equal(33);
                expect(test.duration).to.equal(10);
            });
            it("adds to a list of calls made to the object phone number", () => {
                test.add(10,30);
                test.add(20,60);
                expect(test.calls.length).to.equal(3);
            });
            it("tracks to the total of charges to the object phone number", () => {
                test.add(10,30);
                test.add(20,60);
                expect(test.charge).to.equal(93);
            });
            it("tracks the longest call duration of any individual call made to the object phone number", () => {
                test.add(10,30);
                test.add(20,60);
                expect(test.duration).to.equal(20);
            });
        });
    });
});